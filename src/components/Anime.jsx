import React from 'react';
import { useInstantFetch } from '../hook/useFetchWithCache';
import jikan from '../jikan';
import { NavLink, useParams } from 'react-router-dom';
import Loading from './Loading';

const withoutAutoplay = (url) => {
  const youtubeRegex = /^https?:\/\/(?:www\.)?youtube\.com\/embed\/(.*)$/;
  const match = url.match(youtubeRegex);
  if (match) {
    const videoId = match[1];
    return `https://www.youtube.com/embed/${videoId}?autoplay=0`;
  }
  return url;
};
const Anime = () => {
  // con useParams podemos sacar valores de la urls dinamicas "anime/242"
  const { id } = useParams();
  const { data, loading, error } = useInstantFetch(jikan.getAnimeById, id);

  if (loading) return <Loading />;

  if (error) return 'Something went wrong';

  if (!data) return 'No hay datos disponibles';
  if (data.data)
    return (
      <React.Fragment>
       
        <h1>{data.data.title}</h1>
        <img src={data.data.images.jpg.image_url} alt={data.data.title} />
        <div>Score: {data.data.score}</div>
        <div>Episodes: {data.data.episodes}</div>
        {data.data.source === 'Original' ? (
          <div>Original anime</div>
        ) : (
          <div>Based on a {data.data.source.toLowerCase()}</div>
        )}
        <a href={data.data.url} target="_blank" rel="noopener noreferrer">
          Check it out on MAL!
        </a>
        <p>{data.data.synopsis}</p>
        {data.data.trailer.embed_url && (
          <iframe
            title={`Trailer for ${data.data.title}`}
            width="560"
            height="315"
            src={withoutAutoplay(data.data.trailer.embed_url)}
            frameBorder="0"
            allowFullScreen
          />
        )}
        <NavLink to={`/anime/${Number(id) + 1}`}>Next</NavLink>
      </React.Fragment>
    );

  
};
export default Anime;
